import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of, retry } from 'rxjs';

import { Country } from '../interfaces/pais.interface';
import { Capital } from '../interfaces/capital.interface';
import { Region } from '../interfaces/region.interface';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private apiUrl: string = 'https://restcountries.com/v3.1';

  get params(){
    return new HttpParams().set('fields', 'name,capital,cca2,flags,population');
  }

  constructor(private http: HttpClient) { 

  }

  buscarPais(termino: string): Observable<Country[]>{
    const url = `${this.apiUrl}/name/${termino}`;
    // const params = new HttpParams()
                  // .set('fields', 'name,capital,cca2,flags,population');
    return this.http.get<Country[]>(url, {params: this.params});
  }
 
  obtenerPaisPorId(id: string): Observable<Country[]>{
    const url = `${this.apiUrl}/alpha/${id}`;
    return this.http.get<Country[]>(url);
  }

  buscarCapital(termino: string): Observable<Capital[]>{
    const url = `${this.apiUrl}/capital/${termino}`;
    return this.http.get<Capital[]>(url, { params: this.params });
  }

  buscarRegion(termino: string): Observable<Country[]>{


    const url = `${this.apiUrl}/region/${termino}`;
    return this.http.get<Country[]>(url);
    // .pipe( catchError( (err) => {
    //   console.log('err: ',err.error.message ); 
    //   return of([]);
    // } ));
  }

}
