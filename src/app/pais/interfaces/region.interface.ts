export interface Region {
    name:         Name;
    tld?:         string[];
    cca2:         string;
    ccn3?:        string;
    cca3:         string;
    cioc?:        string;
    independent?: boolean;
    status:       Status;
    unMember:     boolean;
    currencies:   Currencies;
    idd:          Idd;
    capital:      string[];
    altSpellings: string[];
    region:       RegionElement;
    subregion:    Subregion;
    languages:    Languages;
    translations: { [key: string]: Translation };
    latlng:       number[];
    landlocked:   boolean;
    borders?:     string[];
    area:         number;
    demonyms:     Demonyms;
    flag:         string;
    maps:         Maps;
    population:   number;
    gini?:        { [key: string]: number };
    fifa?:        string;
    car:          Car;
    timezones:    string[];
    continents:   RegionElement[];
    flags:        CoatOfArms;
    coatOfArms:   CoatOfArms;
    startOfWeek:  StartOfWeek;
    capitalInfo:  CapitalInfo;
    postalCode?:  PostalCode;
}

export interface CapitalInfo {
    latlng: number[];
}

export interface Car {
    signs: string[];
    side:  Side;
}

export enum Side {
    Left = "left",
    Right = "right",
}

export interface CoatOfArms {
    png?: string;
    svg?: string;
}

export enum RegionElement {
    Europe = "Europe",
}

export interface Currencies {
    EUR?: All;
    HUF?: All;
    MKD?: All;
    NOK?: All;
    UAH?: All;
    PLN?: All;
    CHF?: All;
    RON?: All;
    RSD?: All;
    SEK?: All;
    GBP?: All;
    GGP?: All;
    HRK?: All;
    GIP?: All;
    ISK?: All;
    BYN?: All;
    MDL?: All;
    DKK?: All;
    IMP?: All;
    BGN?: All;
    RUB?: All;
    BAM?: BAM;
    FOK?: All;
    JEP?: All;
    ALL?: All;
    CZK?: All;
}

export interface All {
    name:   string;
    symbol: string;
}

export interface BAM {
    name: string;
}

export interface Demonyms {
    eng:  Eng;
    fra?: Eng;
}

export interface Eng {
    f: string;
    m: string;
}

export interface Idd {
    root:     string;
    suffixes: string[];
}

export interface Languages {
    por?: string;
    deu?: string;
    hun?: string;
    fin?: string;
    swe?: string;
    mkd?: string;
    nor?: string;
    ukr?: string;
    pol?: string;
    bar?: string;
    fra?: string;
    gsw?: string;
    ita?: string;
    roh?: string;
    ron?: string;
    sqi?: string;
    srp?: string;
    eng?: string;
    gle?: string;
    slv?: string;
    ell?: string;
    tur?: string;
    nfr?: string;
    hrv?: string;
    nld?: string;
    isl?: string;
    bel?: string;
    rus?: string;
    mlt?: string;
    lav?: string;
    dan?: string;
    glv?: string;
    cat?: string;
    bul?: string;
    nno?: string;
    nob?: string;
    smi?: string;
    spa?: string;
    bos?: string;
    fao?: string;
    cnr?: string;
    ltz?: string;
    slk?: string;
    nrf?: string;
    lat?: string;
    est?: string;
    lit?: string;
    ces?: string;
}

export interface Maps {
    googleMaps:     string;
    openStreetMaps: string;
}

export interface Name {
    common:     string;
    official:   string;
    nativeName: { [key: string]: Translation };
}

export interface Translation {
    official: string;
    common:   string;
}

export interface PostalCode {
    format: string;
    regex:  string;
}

export enum StartOfWeek {
    Monday = "monday",
}

export enum Status {
    OfficiallyAssigned = "officially-assigned",
    UserAssigned = "user-assigned",
}

export enum Subregion {
    CentralEurope = "Central Europe",
    EasternEurope = "Eastern Europe",
    NorthernEurope = "Northern Europe",
    SoutheastEurope = "Southeast Europe",
    SouthernEurope = "Southern Europe",
    WesternEurope = "Western Europe",
}
