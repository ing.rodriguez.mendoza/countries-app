import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Country } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-pais-suggested',
  templateUrl: './pais-suggested.component.html',
  styles: [
    `
    li {
      cursor: pointer
    }
    `
  ]
})
export class PaisSuggestedComponent implements OnInit {

  @Input() resultSuggested: Country[] = [];
  @Input('searchWord') termino: string = '';
//  @Output() onClickbuscarSugerencias: Country[] = [];
  @Output() onClickbuscarSugerencias: EventEmitter<any> = new EventEmitter();



  constructor() { }

  ngOnInit(): void {
  }

  buscarSugerencias (){
      this.onClickbuscarSugerencias.emit();
  }

}
