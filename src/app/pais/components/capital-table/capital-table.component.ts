import { Component, Input, OnInit } from '@angular/core';
import { Capital } from '../../interfaces/capital.interface';

@Component({
  selector: 'app-capital-table',
  templateUrl: './capital-table.component.html',
  styles: []
})
export class CapitalTableComponent implements OnInit {

  @Input() result: Capital[] = [];
  @Input() option: string = '';

  constructor() { }

  ngOnInit() {
  }

}
