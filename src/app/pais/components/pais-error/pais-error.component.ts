import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pais-error',
  templateUrl: './pais-error.component.html',
  styles: [
  ]
})
export class PaisErrorComponent implements OnInit {

  @Input('searchWord') termino: string ='';

  constructor() { }

  ngOnInit(): void {
  }

}
