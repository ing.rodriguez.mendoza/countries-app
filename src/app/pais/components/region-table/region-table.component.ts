import { Component, Input, OnInit } from '@angular/core';
import { Region } from '../../interfaces/region.interface';

@Component({
  selector: 'app-region-table',
  templateUrl: './region-table.component.html',
  styles: [
  ]
})
export class RegionTableComponent implements OnInit {

  @Input() result: Region[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
