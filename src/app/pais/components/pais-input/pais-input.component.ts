import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { catchError, debounceTime, Subject } from 'rxjs';

@Component({
  selector: 'app-pais-input',
  templateUrl: './pais-input.component.html',
  styles: [
  ]
})
export class PaisInputComponent implements OnInit {

  @Input('wordSearch') termino: string = '';
  @Input() option: string = '';
  @Input() placeholder: string = '';

  
  // @Output() onDebounce: EventEmitter<string> = new EventEmitter();
  @Output() onResult: EventEmitter<any> = new EventEmitter();
  @Output() onSearchWord: EventEmitter<any> = new EventEmitter();
  @Output() onDebouncer: EventEmitter<any> = new EventEmitter();
  
  // @Output() onResult: EventEmitter<Country[]> = new EventEmitter();
  // @Output() onSearchWord: EventEmitter<string> = new EventEmitter();
  // @Output() onSugerencias: EventEmitter<Country[]> = new EventEmitter();
  
  result: any  = [];
  resultSuggested: any = [];

  debounce: Subject<string> = new Subject();
  
  // result: Country[] = [];
  // resultSuggested: Country[] = [];
  
  hayError: boolean = false;

  constructor(private paisService: PaisService) { }

  ngOnInit(): void {
      this.debounce
      .pipe( debounceTime(360) )
      .subscribe(valor => {
        console.log(valor);
        this.onDebouncer.emit(valor);
      })
  }

  teclaPresionada(){
    //emite el valor de ingresado en el input (pq es un observable)
    const valor= this.debounce.next( this.termino )!;    
    // this.buscar();

  }

  sugerencias(valor: string){
    console.log( 'this.option' , this.option);
    console.log( 'this.termino' , this.termino);
    switch (this.option) {
      case '1':
        this.paisService.buscarPais(valor).subscribe(
          {
            next: resp => {
              this.resultSuggested = resp.splice(0,5);
              console.log( 'this.resultSuggested', this.resultSuggested );
              // this.onSugerencias.emit( this.resultSuggested );
            },
            error: err => {
              this.resultSuggested = [];
              console.log( 'this.resultSuggested err', this.resultSuggested );
              // this.onSugerencias.emit( this.resultSuggested );
            }
          }
        )        
        break;    
      case '2':
        this.paisService.buscarCapital(valor).subscribe(
          {
            next: resp => {
              this.resultSuggested = resp.splice(0,5);
              console.log( 'this.resultSuggested', this.resultSuggested );
              // this.onSugerencias.emit( this.resultSuggested );
            },
            error: err => {
              this.resultSuggested = [];
              console.log( 'this.resultSuggested err', this.resultSuggested );
              // this.onSugerencias.emit( this.resultSuggested );
            }
          }
        )
        break;
    
      default:
        break;
    }
  }

  buscar(){
    this.onResult.emit( this.termino );

    // switch (this.option) {
    //   case '1':        
    //     this.paisService.buscarPais(this.termino).subscribe({
    //       next: (resp) => { 
    //         this.result = resp;
    //         this.onResult.emit( this.result );
    //         console.log( 'resp', resp );
    //       }, 
    //       error: (err) => {
    //         console.log( 'error', err );
    //         this.result = [];
    //         this.onResult.emit( this.result );
    //         this.onSearchWord.emit(this.termino);
    //       },
    //     });
    //   break;
    //   case '2':
    //     this.paisService.buscarCapital(this.termino).subscribe({
    //       next: (resp) => { 
    //         this.result = resp;
    //         this.onResult.emit( this.result );
    //         console.log( 'resp', resp );
    //       }, 
    //       error: (err) => {
    //         console.log( 'error', err );
    //         this.result = [];
    //         this.onResult.emit( this.result );
    //         this.onSearchWord.emit(this.termino);
    //       },
    //     });
    //   break;
    
    //   default:
    //     break;
    // }


  }

}  

  /**other way */
  // teclaPresionada(event: any){
  //   console.log( 'event.target.value', event.target.value );
  //   this.buscar();

  // }


// termino: string = '';
// @Output() onEnter: EventEmitter<string> = new EventEmitter();

// buscar(){
//   this.hayError = false;
//   console.log( 'termino', this.termino );
//   this.onEnter.emit( this.termino );
// }
