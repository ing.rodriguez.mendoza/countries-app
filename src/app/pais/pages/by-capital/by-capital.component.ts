import { Component, OnInit } from '@angular/core';
import { Capital } from '../../interfaces/capital.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-by-capital',
  templateUrl: './by-capital.component.html',
  styleUrls: ['./by-capital.component.css']
})
export class ByCapitalComponent implements OnInit {

  // option: string = "2";
  termino: string = "";
  initSearch: boolean = false;
  result: Capital[] = [];
  resultSuggested: Capital[] = [];
  hayError: boolean = false;
  placeholder: string = 'Buscar Capital';

  constructor(private paisService:PaisService) { }

  ngOnInit(): void {
  }

  buscar( termino: string ){
    this.hayError = false;
    this.termino = termino;

    this.paisService.buscarCapital(this.termino).subscribe(
      {
        next: resp => {
          this.result = resp.splice(0,5);
          console.log( 'this.result', this.result );
          // this.onSugerencias.emit( this.resultSuggested );
        },
        error: err => {
          this.result = [];
          console.log( 'this.result', this.result );
          this.hayError = true;
          // this.onSugerencias.emit( this.resultSuggested );
        }
      }
    )
  }

  searchWord(word: string) {
    this.termino = word;
  }

  sugerencias(capitals: Capital[]){
    console.log( 'capitals suggested', capitals );
    this.resultSuggested = capitals;

  }


}
