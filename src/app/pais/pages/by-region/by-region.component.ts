import { Component, OnInit } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { Region } from '../../interfaces/region.interface';
import { Country } from '../../interfaces/pais.interface';
import { catchError, map, of, pluck, throwError } from 'rxjs';
import { AjaxError } from "rxjs/ajax";
@Component({
  selector: 'app-by-region',
  templateUrl: './by-region.component.html',
  styleUrls: ['./by-region.component.css']
})
export class ByRegionComponent implements OnInit {

  termino :string = '';
  result: Country[] = [];
  hayError: boolean = false;
  regionActiva : string ='';
  active: boolean = false;

//  regiones : string[] = [
//  'EU',
//  'EFTA',
//  'CARICOM',
//  'PA',
//  'AU',
//  'USAN',
//  'EEU',
//  'AL',
//  'ASEAN',
//  'CAIS',
//  'CEFTA',
//  'NAFTA',
//  'SAARC']

  regiones = [
    {
    "code": "Africa",
    "name": "Africa"
    }, 
    {
    "code": "America",
    "name": "Americas"
    }, 
    {
    "code": "XXX",
    "name": "Asia"
    }, 
    {
    "code": "Euro",
    "name": "Europe"
    }, 
    {
    "code": "Ocea",
    "name": "Oceania"
    }
  ];

  constructor(private paisService: PaisService) { }

  getClassCSS(region: string): string{
    return (region === this.regionActiva)? 'btn btn-primary': 'btn btn-outline-primary';
  }

  ngOnInit(): void {
    
  }

  handlingErrors = (err: AjaxError) => {
        console.log('err: ',err.message ); 
        this.hayError= true; 
        return of([]);
        // return throwError(err);
  }

  activarRegion(region: string, name: string){
    this.hayError= false; 

    if(name === this.regionActiva) return;

    console.log( 'd nuevo la busqueda' );

    this.regionActiva = name;


    this.active = true;
    this.result = [];
    console.log( 'region', region );

    this.paisService.buscarRegion( region )
        .pipe( //interceptor o filtro antes de la suscripcion => el detector de metales antes de ingresar al aeropuerto
            map( resp => resp)
          , catchError( 
            this.handlingErrors
          //   (err) => {
          //   console.log('err: ',err.error.message ); 
          //   this.hayError= true; 
          //   return of([]);
          //   // return throwError(err);
          // } 
          ))
        .subscribe(
          // (resp) => {
          //   this.result = resp; 
          //   console.log('resp subs: ',resp); 
          // },
          {
            next  : (resp) => {
              this.result = resp; 
              console.log('resp subs: ',resp); 
            },
            error : (err)   => {
              this.result = []; 
              this.hayError= true; 
              console.log('err subs: ',err); 
            }
          }
        );    
  }

}
