import { Component, OnInit } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { Country } from '../../interfaces/pais.interface';
import { catchError, of } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

@Component({
  selector: 'app-by-country',
  templateUrl: './by-country.component.html',
  styleUrls: ['./by-country.component.css']
})
export class ByCountryComponent implements OnInit {

  termino: string = "";
  initSearch: boolean = false;
  result: Country[] = [];
  resultSuggested: Country[] = [];
  hayError: boolean = false;
  haySugerencias: boolean = false;
  hayErrorSugerencias: boolean = false;
  placeholder: string = 'Buscar Pais';

  constructor(private paisService: PaisService) {

  }

  ngOnInit(): void {
  }

  buscar(termino: string){
    this.hayError = false;
    this.termino = termino;

    this.paisService.buscarPais(this.termino).subscribe(
      {
        next: resp => {
          this.result = resp.splice(0,5);
          // console.log( 'this.result', this.result );
          this.haySugerencias = false;
        },
        error: err => {
          this.result = [];
          // console.log( 'this.result', this.result );
          this.hayError = true;
          this.haySugerencias = false;
          // this.onSugerencias.emit( this.resultSuggested );
        }
      }
    )
  }

  searchWord(word: string) {
    this.termino = word;
  }

  atrapaError = ( err: AjaxError ) => {
    console.warn('error en:', err.message);
    this.resultSuggested = [];
    return of();
  }

  sugerencias(country: string){
    // console.log( 'countries suggested', country );
    this.hayErrorSugerencias = false;
    this.haySugerencias = true;
    this.paisService.buscarPais(country)
    .pipe(
      catchError(this.atrapaError)
    )
    .subscribe(
      {
        next: resp => {
          this.resultSuggested = resp.splice(0,3);
          this.termino = country;
          // console.log( 'this.resultSuggested', this.resultSuggested );
          // this.onSugerencias.emit( this.resultSuggested );
        },
        error: err => {
          this.resultSuggested = [];
          this.haySugerencias = false;
          this.hayErrorSugerencias = true;
          this.termino = country;

          // console.log( 'this.resultSuggested err', this.resultSuggested );
          // this.onSugerencias.emit( this.resultSuggested );
        }
      })
  }

  buscarSugerencias (){
      this.buscar(this.termino);
   }

}

  // buscar(termino: string){
  //   this.hayError = false;
  //   this.termino = termino;
  //   console.log( 'termino', this.termino );
  //   const info = this.paisService.buscarPais(this.termino).subscribe({
  //     next: (resp) => { 
  //       this.result = resp;
  //       console.log( 'resp', resp );
  //     }, 
  //     error: (err) => {
  //       console.log( 'error', err );
  //       this.result = [];
  //       this.hayError = true;
  //     },
  //   });
  //   // console.log( 'info', info );
  // }
