import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaisService } from '../../services/pais.service';
import { switchMap, tap } from "rxjs/operators";
import { Country } from '../../interfaces/pais.interface';
import { Translation } from '../../interfaces/capital.interface';

@Component({
  selector: 'app-view-country',
  templateUrl: './view-country.component.html',
  styleUrls: ['./view-country.component.css']
})
export class ViewCountryComponent implements OnInit {


  pais!: Country;
  translationValue!: Translation[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private paisService: PaisService
    ) 
  { }

  ngOnInit(): void {
    
    this.activatedRoute.params
      .pipe(
        switchMap( ({id}) => this.paisService.obtenerPaisPorId(id) ),
        tap( console.log )//permite imprimir la info el producto del observable anterior
      )
      .subscribe(
          pais =>
          {
            this.pais = pais[0];
            const {translations} = pais[0];
            this.translationValue =  Object.values(translations);
          }
        );
  }
}

// translationKey: any;
// this.translationKey = Object.keys(pais[0].translations);
            // console.log( 'resp in view', resp );
            // this.translations = Object.values(pais[0].translations);        
            // console.log( 'pais[0].translations;',pais[0].translations );
            // console.log( 'keys', Object.keys(pais[0].translations) );
            // console.log( 'keys', Object.values(pais[0].translations) );
    // this.activatedRoute.params.subscribe(
    //   ({id}) => {
    //     console.log( 'params', id );
    //     this.paisService.obtenerPaisPorId(id).subscribe(
    //       resp =>
    //       {
    //         console.log( 'resp in view', resp );
    //       }
    //     )
    //   }
    // )