import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ByCapitalComponent } from './pages/by-capital/by-capital.component';
import { ByRegionComponent } from './pages/by-region/by-region.component';
import { ViewCountryComponent } from './pages/view-country/view-country.component';
import { ByCountryComponent } from './pages/by-country/by-country.component';
import { PaisInputComponent } from './components/pais-input/pais-input.component';
import { PaisTableComponent } from './components/pais-table/pais-table.component';
import { PaisErrorComponent } from './components/pais-error/pais-error.component';
import { PaisSuggestedComponent } from './components/pais-suggested/pais-suggested.component';
import { CapitalTableComponent } from './components/capital-table/capital-table.component';
import { RegionTableComponent } from './components/region-table/region-table.component';



@NgModule({
  declarations: [
    ByCapitalComponent,
    ByRegionComponent,
    ViewCountryComponent,
    ByCountryComponent,
    PaisInputComponent,
    PaisTableComponent,
    PaisErrorComponent,
    PaisSuggestedComponent,
    CapitalTableComponent,
    RegionTableComponent
  ],
  exports: [
    ByCapitalComponent,
    ByRegionComponent,
    ViewCountryComponent,
    ByCountryComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
  ]
})
export class PaisModule { }
