import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ByCountryComponent } from "./pais/pages/by-country/by-country.component";
import { ByRegionComponent } from './pais/pages/by-region/by-region.component';
import { ByCapitalComponent } from './pais/pages/by-capital/by-capital.component';
import { ViewCountryComponent } from './pais/pages/view-country/view-country.component';


const routes: Routes = [
    {
      path: '',
      component: ByCountryComponent,
      pathMatch: 'full'//para que sea la ruta principal de mi aplicacion
    },
    {
      path:'region',
      component: ByRegionComponent,      
    },
    {
      path: 'capital',
      component: ByCapitalComponent
    },
    {
      path: 'pais/:id',
      component: ViewCountryComponent
    },
    {
      path: '**',//cualquier otra ruta que ingrese el usuario
      redirectTo: ''
    }
];

@NgModule({
    imports: [
      RouterModule.forRoot(routes)
    ],
    exports: [
      RouterModule
    ]
})
export class AppRoutingModule {

}